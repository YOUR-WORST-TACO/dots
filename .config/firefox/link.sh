#!/usr/bin/env bash

FIREFOX_LOC="$HOME/.mozilla/firefox"

FIREFOX_PROF="$(grep 'Default=' $FIREFOX_LOC/profiles.ini | sed s/^Default=// | head -1)"

if [ -z "$FIREFOX_PROF" ]; then
	echo "No profile detected"
fi

if [ ! -d "$FIREFOX_LOC/$FIREFOX_PROF/chrome" ]; then
	mkdir "$FIREFOX_LOC/$FIREFOX_PROF/chrome"
fi

ln -sf "$(pwd)/userChrome.css" "$FIREFOX_LOC/$FIREFOX_PROF/chrome/userChrome.css"