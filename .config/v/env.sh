#   █      ██         █                          █                               
#   █      █          █   █                                                   █  
# ███ ███ ███ ███ █ █ █  ███   █ █   ███ ███ █ █ █ ███ ███ ███ █████ ███ ███ ███ 
# █ █ ███  █    █ █ █ █   █    █ █   ███ █ █ █ █ █ █   █ █ █ █ █ █ █ ███ █ █  █  
# █ █ █    █  ███ █ █ █   █    █ █   █   █ █ █ █ █ █   █ █ █ █ █ █ █ █   █ █  █  
# ███ ███  █  ███ ███ ██  ██    █    ███ █ █  █  █ █   ███ █ █ █ █ █ ███ █ █  ██ 
#                                                                                
# Everything defined here is required for smooth operation of the system

export PATH="/home/taco/.bin:$PATH"

# Ruby bin folder
export PATH="$PATH:/home/taco/.gem/ruby/2.7.0/bin"

# Fix jetbrains products
export _JAVA_AWT_WM_NONREPARENTING=1

# Custom install of dotnet
export DOTNET_ROOT="/home/taco/.dotnet"
export PATH="$PATH:/home/taco/.dotnet"

# Golang path
export GOPATH="/home/taco/.go"

# Fix firefox scrolling problems
export MOZ_USE_XINPUT2=1