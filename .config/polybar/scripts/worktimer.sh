#!/bin/bash

state=0
timer=0
sleep_pid=0

output_directory="$HOME/LiveACID/Hours"

secondstoformat() {
	H=$(($1/60/60))
	M=$(($1/60%60))
	S=$(($1%60))

	printf '%02d:%02d:%02d\n' $H $M $S
}

formattoseconds() {
	S=$((($1*60*60) + ($2*60) + $3))

	echo "$S"
}

toggle() {
	state=$(((state + 1) % 2))

	if [ "$sleep_pid" -ne 0 ]; then
		kill $sleep_pid >/dev/null 2>/dev/null
	fi
}

reset() {

	if [ $state -eq 1 ]; then
		state=0
	else
		file_name=$(echo "$(date +%A-%b-%d)" | awk '{print tolower($file)}')

		if [ ! -d $output_directory ]; then
			mkdir -p $output_directory
		fi

		if [ ! -f "$output_directory/$file_name" ]; then
			echo "00:00:00" > "$output_directory/$file_name"
		fi

		file_contents=$(<"$output_directory/$file_name")

		seconds=${file_contents##*:}
		file_contents=${file_contents%:*}
		minutes=${file_contents##*:}
		hours=${file_contents%:*}

		only_seconds=$(formattoseconds $hours $minutes $seconds)

		timer=$((timer + only_seconds))

		secondstoformat $timer > "$output_directory/$file_name"
		
		timer=0
	fi

	if [ "$sleep_pid" -ne 0 ]; then
		kill $sleep_pid >/dev/null 2>/dev/null
	fi
}

trap "toggle" USR1
trap "reset" USR2

while true; do
	if [ $state -eq 1 ]; then
		timer=$((timer + 1))
	fi
	# code

	secondstoformat $timer

	sleep 1 &
	sleep_pid=$!
	wait
done