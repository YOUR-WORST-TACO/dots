#!/usr/bin/env bash

WIRELESS_INTERFACE="/sys/class/net/wlp61s0"
WIRED_INTERFACE="/sys/class/net/enp0s31f6"

WIRELESS_STATUS=false
WIRED_STATUS=false

if grep -Fxq up "$WIRELESS_INTERFACE/operstate"
then
	WIRELESS_STATUS=true
fi

if grep -Fxq up "$WIRED_INTERFACE/operstate"
then
	WIRED_STATUS=true
fi

if [ $WIRELESS_STATUS = true ]; then
	printf " 直  "
else
	printf " 睊  "
fi

if [ $WIRED_STATUS = true ]; then
	printf "\n"
else
	printf "\n"
fi