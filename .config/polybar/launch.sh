#!/usr/bin/env bash

killall -q polybar

while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

polybar right >>/tmp/polybar-right.log 2>&1 &
polybar center >>/tmp/polybar-center.log 2>&1 &
polybar left >>/tmp/polybar-left.log 2>&1 &