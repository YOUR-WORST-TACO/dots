# Wallpapers

## Reflection

![Reflection](https://www.hdwallpapers.net/previews/reflection-polyscape-1085.jpg)

Wallpaper: [link](https://www.hdwallpapers.net/abstract/reflection-polyscape-wallpaper-1085.htm)

## Snow-Lake

![Snow-Lake](https://cdna.artstation.com/p/assets/images/images/006/927/312/medium/alena-aenami-quiet-1px.jpg?1502309239)

Wallpaper: [link](https://www.artstation.com/artwork/Y20RX)

## Pine-Lake

![Pine-Lake](https://preview.redd.it/td28fr2zvs341.jpg?width=960&crop=smart&auto=webp&s=f6300869e82144c48a68f993bef43d5a45ab3d3e)

Wallpaper: [link](https://www.reddit.com/r/wallpapers/comments/e8q2bp/painted_scenery/)

## Chinese-Dragon

![Chinese-Dragon](https://cdna.artstation.com/p/assets/images/images/022/834/528/large/anato-finnstark-dd6t2du-37e12969-f8bd-4765-b17c-a3a1f067df08.jpg?1576857024)

Wallpaper: [link](https://www.artstation.com/artwork/Qz3nxl)

## Lava-Pink

![Lava-Pink](https://preview.redd.it/r48ej0qzgef41.jpg?width=960&crop=smart&auto=webp&s=1bfb933768543df8d7481d7a3a895618e2c5210b)

Wallpaper: [link](https://www.reddit.com/r/wallpapers/comments/f02hu6/landscapes/)

## Maroon Bells

Edited with Gimp

![Maroon-Bells](/.walls/maroon-bells.jpg)

Wallpaper: [link](https://www.wallpaperu3.com/maroon-bells-wallpaper-background.html)

## https://i.redd.it/zkb3pjhb9co41.png

![The-End](https://i.redd.it/zkb3pjhb9co41.png)

Wallpaper: [link](https://www.reddit.com/r/wallpapers/comments/fncwax/the_beginning_of_the_end_by_artem_chebokha/)