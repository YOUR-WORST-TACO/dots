# V

## NOTE, PENDING CHANGES

![Preview](/.config/v/example/desktop.png)

## Dependencies

The dependencies below are required and used in this rice.

### Normal

* sxhkd
* polybar
* zsh
* figlet
* rofi

### Special

* [bspwm Javyre fork](https://github.com/javyre/bspwm/tree/round_corners)
* [picom ibhagwan fork](https://github.com/ibhagwan/picom)
* [wpgtk](https://github.com/deviantfero/wpgtk)
* [i3lock-color](https://github.com/PandorasFox/i3lock-color)
* [libinput-gestures](https://github.com/bulletmark/libinput-gestures)
* [fusuma](https://github.com/iberianpig/fusuma)
* [eww](https://github.com/elkowar/eww.git)

### Cursor Theme

* [Bibata Cursor](https://github.com/Kaizlqbal/Bibata_Cursor)

### Font

* [Iosevka Term](https://github.com/Be5invis/Iosevka)
* [Nerd Fonts](https://github.com/ryanoasis/nerd-fonts)

## Wallpaper
Go to the wallpaper link for full resolution

![Wallpaper](https://www.hdwallpapers.net/previews/reflection-polyscape-1085.jpg)

Wallpaper: [link](https://www.hdwallpapers.net/abstract/reflection-polyscape-wallpaper-1085.htm)