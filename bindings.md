```
# █   █       █ █             
# █           █               
# ███ █ ███ ███ █ ███ ███ ███ 
# █ █ █ █ █ █ █ █ █ █ █ █ █   
# █ █ █ █ █ █ █ █ █ █ █ █   █ 
# ███ █ █ █ ███ █ █ █ ███ ███ 
#                       █     
#                     ███     

# Apps
super + enter:                      terminal
super + d:                          select_kitty

# WM Functions
super + r:                          run dialog
super + shift + r:                  drun dialog
super + c:                          kill
super + shift + c:                  kill hard
super + t:                          tiled
super + shift + t:                  pseudo tiled
super + s:                          floating
super + f:                          fullscreen
super + k:                          display keybinds
suber + w:                          change wallpaper
super + b:                          toggle statusbar
super + escape:                     logout dialog
super + l:                          lock

# WM Tiling
super + g:                          swap biggest

# WM Navigation
super + <direction>:                select node in direction
super + shift + <direction>:        swap with node in direction
super + space:                      toggle selected node forward
super + shift + space:              toggle selected node backward

# WM Desktops
super + ]:                          next desktop
super + [:                          previous desktop
super + ~:                          last node
super + tab:                        last desktop
super + [1-7]:                      focus specific desktop
super + shift + [1-7]:              move node to desktop

# WM Preselection
super + ctrl + <direction>:         preselect next node
super + ctrl + [1-9]:               change preselect ratio
super + ctrl + space:               cancel preselect

# WM Resizing
super + alt + <direction>:          expand a window
super + alt + shift + <direction>:  contract a window
alt + space + <direction>:          move floating window

# System Utilities
print:                              capture print screen
ctrl + print:                       select capture print screen

```