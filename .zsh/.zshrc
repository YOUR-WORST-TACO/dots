###################################
# VERSION:1.2.5
#   ███ ███ █ █         
#     █ █   █ █ ███ ███ 
#    █   █  ███ █   █   
#   █     █ █ █ █   █   
# █ ███ ███ █ █ █   ███ 
# zshrc file for tayko
# does crazy shit,
# "who needs oh-my-zsh?"
# 				- taco
#
# Ctrl+F !<layout>, ie:
# 	Ctrl+F !colors
#
# File Layout:
#  - Colors
#  - Notify
#  - Update
#  - Zsh Auto Setup
#  - Plugins
#  - Config
#  - Binds
#  - Alias
#  - Func's
#  - Per OS
#  - Dots
#  - Prompt
###################################

export ZSH="$HOME/.zsh"
export ZSH_PL="$ZSH/plugins"
export ZSH_COMP="$ZSH/completion"
export ZSH_EXTRA="$HOME/.zshextra"

export ZSH_UPDATE=""

# Fix for scrolling in bat
pager='less -RS'
export PAGER="$pager"
export BAT_PAGER="$pager"

export DOTS_DIR="$HOME/.dots"
export MICRO_BIN="$ZSH/micro"

###################################
#         █              
# ███     █              
# █   ███ █  ███ ███ ███ 
# █   █ █ █  █ █ █   █   
# █   █ █ █  █ █ █     █ 
# ███ ███ ██ ███ █   ███  
# set F and B for color codes
# !colors
###################################

RESET='\033[0m'

#   F - Foreground
#   1          2          3          4          5          6          7          8
#   9          10         11         12         13         14         15         16
F=(
	'\033[30m' '\033[31m' '\033[32m' '\033[33m' '\033[34m' '\033[35m' '\033[36m' '\033[37m'
	'\033[90m' '\033[91m' '\033[92m' '\033[93m' '\033[94m' '\033[95m' '\033[96m' '\033[97m'
)

#   B - Background
#   1          2          3          4          5          6          7          8
#   9          10         11         12         13         14         15         16
B=(
	'\033[40m' '\033[41m' '\033[42m' '\033[43m' '\033[44m' '\033[45m' '\033[46m' '\033[47m'
	'\033[100m' '\033[101m' '\033[102m' '\033[103m' '\033[104m' '\033[105m' '\033[106m' '\033[107m'
)


###################################
#               █  ██     
# █   █      █     █      
# ██  █ ███ ███ █ ███ █ █ 
# █ █ █ █ █  █  █  █  █ █ 
# █  ██ █ █  █  █  █  █ █ 
# █   █ ███  ██ █  █  ███ 
#                       █ 
#                     ███ 
# some helper functions to handle
# notifications for zshrc
# !notify
###################################

function NOTIFY() {
	echo "${F[11]}[INFO]$RESET $@"
}

function NOTIFYn() {
	echo -n "${F[11]}[INFO]$RESET $@"
}

function WARN() {
	echo "${F[10]}[WARN]$RESET $@"
}

###################################
# ZSH Auto setup
# Will automatically setup zsh 
# when run for the first time
###################################

zsh_config_backup=(
	"$HOME/.zshenv"
	"${HISTFILE:-$HOME/.zsh_history}"
)

zsh_config_move=(
	"$HOME/.zprofile"
	"$HOME/.zlogin"
	"$HOME/.zlogout"
	"$HOME/.zshrc"
)

if [ ! -f "$ZSH/.zshrc" ]; then
	mkdir -p "$ZSH/backup"
	mv "${zsh_config_backup[@]}" "$ZSH/backup" 2>/dev/null
	mv "${zsh_config_move[@]}" "$ZSH/" 2>/dev/null

	rm -f "$HOME/.zcompdump*"

	export ZDOTDIR=$"HOME/.zsh"
	export HISTFILE="$HOME/.zsh/.history"

	cat <<EOF > "$HOME/.zshenv"
export ZDOTDIR="$HOME/.zsh"
export HISTFILE="$HOME/.zsh/.history"
EOF
fi


###################################
#           █             
# █ █       █      █      
# █ █ ███ ███ ███ ███ ███ 
# █ █ █ █ █ █   █  █  ███ 
# █ █ █ █ █ █ ███  █  █   
# ███ ███ ███ ███  ██ ███ 
#     █                   
#     █                   
# handle updating zsh script on 
# launch
# !update
###################################

# TODO make backend version checking silent or something?
# 	   possibly do this by storing an update file that is read when the terminal starts again.

function _handle_zsh_update() {
	ZSH_LOCAL="$ZSH/.zshrc"
	ZSH_SOURCE="$ZSH/.zshrc.src"

	ZSH_TIMESTAMP="$ZSH/.lastcheck"

	touch -a "$ZSH_TIMESTAMP"

	ZSH_LAST_VERSION_CHECK=$(date -d "$(stat -c %y $ZSH_TIMESTAMP)" +%s)
	ZSH_CURRENT_CHECK_DATE=$(date +%s)

	if [[ $ZSH_CURRENT_CHECK_DATE -ge $ZSH_LAST_VERSION_CHECK ]]; then
		NOTIFY "Checking for zshrc update."
		touch -d "$(date -d '+1 hour')" "$ZSH_TIMESTAMP"
		if ping -q -w1 -c1 google.com > /dev/null 2> /dev/null; then
			curl -sL https://gitlab.com/YOUR-WORST-TACO/dots/-/raw/v/.zsh/.zshrc -o "$ZSH_SOURCE"

			ZSH_LOCAL_VERSION=$(sed '2!d' "$ZSH_LOCAL")
			ZSH_LOCAL_VERSION=${ZSH_LOCAL_VERSION### VERSION:}
	
			ZSH_SOURCE_VERSION=$(sed '2!d' "$ZSH_SOURCE")
			ZSH_SOURCE_VERSION=${ZSH_SOURCE_VERSION### VERSION:}

			if [[ ! $(printf '%s\n' "$ZSH_SOURCE_VERSION" "$ZSH_LOCAL_VERSION" | sort -V | head -n1) = "$ZSH_SOURCE_VERSION" ]]; then

				echo -en "\033[s"
				echo -en "\033[1A"
				echo -en "\r"
				echo -en "\033[2K"
				
				NOTIFYn "Newer version of .zshrc found, updating... (v$ZSH_LOCAL_VERSION -> v$ZSH_SOURCE_VERSION)"

				echo -en "\033[u"

				rm "$ZSH_LOCAL"
				mv "$ZSH_SOURCE" "$ZSH_LOCAL"
				export ZSH_UPDATE="$ZSH_SOURCE_VERSION"
			else
				rm "$ZSH_SOURCE"
			fi
		fi

	fi

	unset ZSH_LOCAL
	unset ZSH_SOURCE
	unset ZSH_LOCAL_VERSION
	unset ZSH_SOURCE_VERSION
}

(_handle_zsh_update &)

unset _handle_zsh_update


function reloadzsh() {
	clear
	NOTIFY "Reloading .zshrc..."
	source "$ZSH/.zshrc"
}


###################################
#     █          █         
# ███ █                    
# █ █ █  █ █ ███ █ ███ ███ 
# ███ █  █ █ █ █ █ █ █ █   
# █   █  █ █ █ █ █ █ █   █ 
# █   ██ ███ ███ █ █ █ ███ 
#              █           
#            ███           
# defines and installs plugins
# and scripts
# !plugins
###################################

# Fix issue with missing terminfo on server
if ! infocmp >/dev/null 2>/dev/null; then
	WARN "Server does not support current terminfo, using xterm-256color instead."
	export TERM='xterm-256color'
fi

plugin_files=()
function psource() {
	plugin_files+=( "$1" )
}

# ZSH AUTO SUGGESTIONS
PL_autosuggestions="$ZSH_PL/zsh-autosuggestions"
[ ! -d "$PL_autosuggestions" ] &&
{
	NOTIFY "installing ${F[12]}Zsh Auto Suggestions$RESET"
	git clone "https://github.com/zsh-users/zsh-autosuggestions" "$PL_autosuggestions"
}
psource "$PL_autosuggestions"/zsh-autosuggestions.zsh

# fixes for widget binding below
ZSH_AUTOSUGGEST_ACCEPT_WIDGETS+=('key-right' 'key-send' 'key-send2')
ZSH_AUTOSUGGEST_CLEAR_WIDGETS+=('up-line-or-beginning-search' 'down-line-or-beginning-search')
# ZSH_AUTOSUGGEST_CLEAR_WIDGETS+=('key-sup' 'key-sdown') <- was breaking with latest version of autosuggestions
ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE="fg=10,bold"

# limit autosuggestion size
ZSH_AUTOSUGGEST_BUFFER_MAX_SIZE=20

# ZSH SYNTAX HIGHLIGHTING
PL_syntax_highlighting="$ZSH_PL/zsh-syntax-highlighting"
[ ! -d "$PL_syntax_highlighting" ] &&
{
	NOTIFY "installing ${F[12]}Zsh Syntax Highlighting$RESET"
	git clone "https://github.com/zsh-users/zsh-syntax-highlighting" "$PL_syntax_highlighting"
}
psource "$PL_syntax_highlighting"/zsh-syntax-highlighting.zsh

# ZSH 256 COLOR
PL_256color="$ZSH_PL/zsh-256color"
[ ! -d "$PL_256color" ] &&
{
	NOTIFY "installing ${F[12]}Zsh 256 Color$RESET"
	git clone "https://github.com/chrissicool/zsh-256color" "$PL_256color"
}
psource "$PL_256color"/zsh-256color.plugin.zsh

# ZSH COLORED MAN PAGES
PL_colored_man_pages="$ZSH_PL/zsh-colored-man-pages"
[ ! -d "$PL_colored_man_pages" ] &&
{
	NOTIFY "installing ${F[12]}Zsh Colored Man Pages$RESET"
	git clone "https://github.com/ael-code/zsh-colored-man-pages" "$PL_colored_man_pages"
}
psource "$PL_colored_man_pages"/colored-man-pages.plugin.zsh

# ZSH ALIAS TIPS
PL_alias_tips="$ZSH_PL/zsh-alias-tips"
[ ! -d "$PL_alias_tips" ] &&
{
	NOTIFY "installing ${F[12]}Zsh Alias Tips$RESET"
	git clone "https://github.com/djui/alias-tips.git" "$PL_alias_tips"
}
psource "$PL_alias_tips"/alias-tips.plugin.zsh

# ZSH COMPLETIONS
PL_completions="$ZSH_PL/zsh-completions"
[ ! -d "$PL_completions" ] &&
{
	NOTIFY "installing ${F[12]}Zsh Completions$RESET"
	git clone "https://github.com/zsh-users/zsh-completions" "$PL_completions"
}
fpath=("$PL_completions"/src $fpath)

# GITHUB PROMPT
PL_git_prompt="$ZSH_PL/git-prompt.sh"
[ ! -f "$PL_git_prompt" ] &&
{
	NOTIFY "installing ${F[12]}Git Prompt$RESET"
	curl "https://raw.githubusercontent.com/git/git/master/contrib/completion/git-prompt.sh" --output "$PL_git_prompt"
}
psource "$PL_git_prompt"

# GITHUB COMPLETIONS
[ ! -f "$ZSH"/git-completion.bash ] &&
{
	NOTIFY "installing ${F[12]}Zsh Github Completions$RESET"
	curl "https://raw.githubusercontent.com/git/git/master/contrib/completion/git-completion.bash" --output "$ZSH"/git-completion.bash
}

zstyle ':completion:*:*:git:*' script "$ZSH"/git-completion.bash

[ ! -f "$ZSH_COMP"/_git ] &&
{
	curl "https://raw.githubusercontent.com/git/git/master/contrib/completion/git-completion.zsh" --create-dirs --output "$ZSH_COMP"/_git
}

fpath=("$ZSH_COMP" $fpath)


###################################
#              ██ █     
# ███          █        
# █   ███ ███ ███ █ ███ 
# █   █ █ █ █  █  █ █ █ 
# █   █ █ █ █  █  █ █ █ 
# ███ ███ █ █  █  █ ███ 
#                     █ 
#                   ███ 
# Set up completions, options
# and history
# !config
###################################

autoload -U compdef compaudit compinit
compinit

zstyle ':completion:*:*:*:*:*' menu select

setopt prompt_subst

setopt auto_cd
setopt auto_menu

setopt always_to_end
setopt complete_in_word
# setopt complete_aliases

HISTSIZE=50000
SAVEHIST=50000

setopt extended_history
setopt hist_expire_dups_first
setopt inc_append_history
setopt hist_verify
setopt hist_ignore_space

unsetopt share_history

if type kubectl > /dev/null; then
	source <(kubectl completion zsh)
fi

###################################
#     █       █     
# ██          █     
# █ █ █ ███ ███ ███ 
# ██  █ █ █ █ █ █   
# █ █ █ █ █ █ █   █ 
# ██  █ █ █ ███ ███ 
# sets up zle and normal keybinds
# !binds
###################################

autoload -U up-line-or-beginning-search
zle -N up-line-or-beginning-search
bindkey "${terminfo[kcuu1]}" up-line-or-beginning-search

autoload -U down-line-or-beginning-search
zle -N down-line-or-beginning-search
bindkey "${terminfo[kcud1]}" down-line-or-beginning-search

if (( ${+terminfo[smkx]} )) && (( ${+terminfo[rmkx]} )); then
	function zle-line-init() {
		echoti smkx
	}

	function zle-line-finish() {
		echoti rmkx
	}

	zle -N zle-line-init
	zle -N zle-line-finish
fi

bindkey -e

bindkey "^[OH" beginning-of-line
bindkey "^[OF" end-of-line
bindkey "^[[3~" delete-char


###################################
# Line editing solution
# Solution from:
# 	https://stackoverflow.com/questions/5407916/zsh-zle-shift-selection
###################################

r-delregion() {
  if ((REGION_ACTIVE)) then
     zle kill-region
  else 
    local widget_name=$1
    shift
    zle $widget_name -- $@
  fi
}

r-deselect() {
  ((REGION_ACTIVE = 0))
  local widget_name=$1
  shift
  zle $widget_name -- $@
}

r-select() {
  ((REGION_ACTIVE)) || zle set-mark-command
  local widget_name=$1
  shift
  zle $widget_name -- $@
}

for key     kcap   seq        mode   widget (
    sleft   kLFT   $'\e[1;2D' select   backward-char
    sright  kRIT   $'\e[1;2C' select   forward-char
    sup     kri    $'\e[1;2A' select   up-line-or-history
    sdown   kind   $'\e[1;2B' select   down-line-or-history

    send    kEND   $'\E[1;2F' select   end-of-line
    send2   x      $'\E[4;2~' select   end-of-line

    shome   kHOM   $'\E[1;2H' select   beginning-of-line
    shome2  x      $'\E[1;2~' select   beginning-of-line

    left    kcub1  $'\EOD'    deselect backward-char
    right   kcuf1  $'\EOC'    deselect forward-char

    end     kend   $'\EOF'    deselect end-of-line
    end2    x      $'\E4~'    deselect end-of-line

    home    khome  $'\EOH'    deselect beginning-of-line
    home2   x      $'\E1~'    deselect beginning-of-line

    csleft  x      $'\E[1;6D' select   backward-word
    csright x      $'\E[1;6C' select   forward-word
    csend   x      $'\E[1;6F' select   end-of-line
    cshome  x      $'\E[1;6H' select   beginning-of-line

    cleft   x      $'\E[1;5D' deselect backward-word
    cright  x      $'\E[1;5C' deselect forward-word

    del     kdch1   $'\E[3~'  delregion delete-char
    bs      x       $'^?'     delregion backward-delete-char

  ) {
  eval "key-$key() {
    r-$mode $widget \$@
  }"
  zle -N key-$key
  bindkey ${terminfo[$kcap]-$seq} key-$key
}


###################################
#     █  █         
# ███ █            
# █ █ █  █ ███ ███ 
# █ █ █  █   █ █   
# ███ █  █ ███   █ 
# █ █ ██ █ ███ ███ 
# all my standard aliases
# !alias
###################################

alias cp="cp -ir"
alias mv="mv -i"

alias ls='ls -F --color=always --group-directories-first'
alias ll='ls -lah'
alias ld='ls -d */'
alias la='ls -CAh'
alias l='ls'
alias sl='ls'

alias c='cd'

alias reload="clear && source $ZSH/.zshrc"
alias sudo='nocorrect sudo'

alias search="fzf --preview 'bat --color=always --style=numbers --line-range=:500 {}'"

# Configure kubectl completions and add k as an alias
alias k='kubectl'
compdef k='kubectl'

alias s='sudo'
alias icat='kitty +kitten icat'
alias myip="curl http://ipecho.net/plain; echo"

alias free='free -ht'

alias dm='sudo mount -o umask="0022,gid=$(id -g),uid=$(id -u)"'

alias gitl="git log -n 20 --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit"

alias grep='grep --color=always'

alias -g NE='2>/dev/null'
alias -g GR='| grep --color=always'
alias -g NO='2>&1 >/dev/null'

alias -g PASS='"$(read -rs "pw?Password: " && printf "$pw" && unset pw)"'

[ "$TERM" = "xterm-kitty" ] &&
	alias ssh='kitty +kitten ssh'


###################################
#                 █     
# ███             █     
# █   █ █ ███ ███   ███ 
# ███ █ █ █ █ █     █   
# █   █ █ █ █ █       █ 
# █   ███ █ █ ███   ███ 
# functions for general command
# line use.
# !func's
###################################

# creates a docker container for testing
# handles opening and reopening containers
function tester() {
	if [ "$#" -eq 0 ]; then
		used_names=()
		for tester_name ( $(docker ps --filter "label=tester.node" --format "{{.Names}}" | grep "tester-" ) ) {
			used_names+=( "$(cut -d "-" -f 2 <<< $tester_name)" )
		}

		container_name_random="$(( (RANDOM % 9000) + 1000 ))"
		while [[ ${used_names[(ie)$container_name_random]} -le ${#used_names} ]]; do
			container_name_random="$(( (RANDOM % 9000) + 1000 ))"
		done
	
		container_name="tester-$container_name_random"
				
		docker run -it -d --rm --name=$container_name --label tester.node=true --hostname=$container_name archlinux > /dev/null 2>&1
		docker exec -it $container_name /bin/bash
	elif [[ "$#" -eq 1 ]]; then
	
		current_testers=()
		for tester_name ( $(docker ps --filter "label=tester.node" --format "{{.Names}}" ) ) {
			current_testers+=( "$tester_name" )
		}
		
		if [[ "$1" =~ ^[0-9]+$ ]]; then
			if [[ ${#current_testers} -ge $1 ]]; then
				docker exec -it ${current_testers[(( $1 + 1 ))]} /bin/bash
			else
				echo "Could not find tester with index $1."
			fi
		else
			if [[ ${current_testers[(ie)$1]} -le ${#current_testers} ]]; then
				docker exec -it $1 /bin/bash
			else
				docker run -it -d --rm --name=$1 --label tester.node=true --hostname=$1 archlinux > /dev/null 2>&1
				docker exec -it $1 /bin/bash
			fi
		fi
	fi
}

# deletes tester by id or index
function rmtester() {
	if [ "$#" -eq 0 ]; then
		testers
	else
		current_testers=()
		
		for tester_name ( $(docker ps --filter "label=tester.node" --format "{{.Names}}" ) ) {
			current_testers+=( "$tester_name" )
		}
		
		while [ ! "$#" -eq 0 ]; do
			if [[ "$1" =~ ^[0-9]+$ ]]; then
				docker kill ${current_testers[(( $1 + 1 ))]}
			else
				if [[ ${current_testers[(ie)$1]} -le ${#current_testers} ]]; then
					docker kill $1
				else
					echo "Could not kill tester identity: $1"
				fi
			fi
			shift
		done
	fi
}

# lists running tester containers
function testers() {
	tester_index=0
	{
		[ ! "$1" = "off" ] &&
			echo "Index Name ID"
			
		for tester_name tester_id ( $(docker ps --filter "label=tester.node" --format "{{.Names}} {{.ID}}" ) ) {
			echo "$tester_index $tester_name $tester_id"
			(( tester_index++ ))
		}
	} | column -t
}

# Automatically installs micro
# if not found.
if ! type micro > /dev/null; then
	function micro() {
		mkdir "$MICRO_BIN"
		curl "https://getmic.ro" -o "$MICRO_BIN"/installer
		chmod +x "$MICRO_BIN"/installer
		NOTIFY "Installing Micro, requires sudo permissions..."
		(
			cd "/usr/local/bin" && sudo "$MICRO_BIN"/installer
		)
		#	bash "$MICRO_BIN"/micro &
	
		"/usr/local/bin"/micro "$@"
		unfunction micro
	}
fi

###################################
# Elevate Previous Command
# https://gist.github.com/ntpeters/ca6a3a0cccf075a0bc8c
###################################

function elevate_previous() {
	# Check if running with ZSH history verification
	if [ -z ${HIST_VERIFY+x} ]; then
		setopt no_histverify
		sudo `fc -ln -1`
		setopt histverify
	else
		sudo `fc -ln -1`
	fi
}
alias ffs=elevate_previous

# A secret
function secrets() {
	if [ "$#" -eq 0 ]; then
		NOTIFY "*The magic door makes an ominous sound*"
	elif [ "$#" -gt 1 ]; then
		NOTIFY "*Your candle flickers from a sudden gust of wind*"
	else
		if [ -f "./.secrets.tomb" ]; then
			tomb open -k "$1" ./.secrets.tomb
			sed -i '/secrets/d' $HISTFILE
		else
			NOTIFY "*You hear a footsteps approaching behind you*"
		fi
	fi
}

function purge() {
	NOTIFY "History ${F[10]}PURGED...$RESET"
	rm ${ZSH}/.history
}

###################################
# ███           ███ ███ 
# █ █ ███ ███   █ █ █   
# ███ ███ █     █ █  █  
# █   █   █     █ █   █ 
# █   ███ █     ███ ███ 
# uses functions and a switch
# to hand out os specific
# configs
# !per os
###################################

function arch() {
	alias up='sudo pacman -Syu'
}

function ubuntu() {
	alias up='sudo apt update && sudo apt upgrade'
}

if [ -f /etc/os-release ]; then
	OS_NAME=$(awk -F= '$1=="ID" { print $2 ;}' /etc/os-release)

	case "$OS_NAME" in
		"arch")
			arch
			;;
		"ubuntu")
			ubuntu
			;;
	esac
fi

alias _updoot_up=up

function updoot() {
	clear
	cat <<EOF
	⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⢈⠈⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
	⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⣬⣿⣿⣿⣿⣿⣿⣿⣎⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
	⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣠⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣯⠌⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
	⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣰⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⢎⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
	⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⡱⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡷⠳⡣⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
	⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠰⣷⣿⣿⣿⣿⡷⣿⣿⠏⠀⠀⢀⠈⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
	⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⡰⣿⠁⠀⠀⠀⣷⠑⠁⠀⢀⡼⠁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
	⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⡀⠹⢌⠈⢀⣈⣾⠌⠀⣷⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
	⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠑⣝⠙⢉⣸⢈⣌⣬⣿⠀⠀⠀⠈⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
	⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠐⡃⠙⠳⡷⡷⠳⠓⠀⣀⢀⠁⠀⡈⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
	⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣌⣌⡮⠇⠌⠷⢀⠾⠀⠀⢈⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
	⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣿⠎⣾⠑⡢⢎⠈⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
	⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣻⡌⠙⢀⠮⠰⣷⡮⠀⠀⠑⠀⠑⠠⠄⠀⠀
	⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣱⠏⣿⣄⣮⠀⠖⢀⣬⢌⠀⠀⠀⠀⢀⣍⠀
	⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⡰⠏⠏⣾⣁⠌⠀⡰⣿⣿⣿⣯⣬⣿⣿⣿⠀
	⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢣⠁⠃⠐⣃⣊⣿⣿⣿⣿⣿⣿⣿⣿⠿⠀
	⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢱⣿⠮⠀⠐⣷⣿⣿⣿⣿⣿⣿⡿⠗⠀⠀
	⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠱⠳⠳⠳⠳⠑⠀⠀⠀⠀
	       ⠀⠀⠀⠀⠀⠀⢈⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
	       ⠀⠀⠀⠀⠀⠀⣿⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣸⠎⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
	       ⠀⣨⡾⠳⠳⣦⣿⠀⠀⣈⡮⠳⠳⡣⣎⠀⠀⣀⡮⠳⠳⡣⣎⠀⠀⠲⠳⣿⠿⠳⠳⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
	       ⠀⣿⠀⠀⠀⠐⣿⠀⠀⣿⠀⠀⠀⠀⣳⠏⠀⣿⠁⠀⠀⠀⣱⠏⠀⠀⠀⣿⠏⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
	       ⠀⣷⢌⠀⠀⣨⣿⠀⠀⣳⢎⠀⠀⢈⡿⠃⠀⣳⢎⠀⠀⢀⣾⠃⠀⠀⠀⣷⢏⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
	       ⠀⠀⠑⠳⠓⠁⠳⠀⠀⠀⠑⠱⠳⠑⠀⠀⠀⠀⠑⠱⠳⠑⠁⠀⠀⠀⠀⠐⠑⠳⠳⠁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
	⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
	       ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣿⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣀⠌⠀⠀⠀⠀
	       ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⣌⡌⡄⢌⣿⠀⠀⠀⣈⡌⡄⣌⢌⠀⠀⠀⣈⣌⡄⣄⢌⠀⠀⡀⡄⣾⡯⡄⡄⠄⠀
	⠀⠀⠀       ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣾⠗⠀⠀⠐⣿⠀⠀⣸⠟⠀⠀⠀⣱⠏⠀⣰⠟⠀⠀⠀⣱⢏⠀⠀⠀⣰⠏⠀⠀⠀⠀
	⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀       ⠀⠀⠀⠀⠀⠀⠀⣿⠌⠀⠀⠀⣿⠀⠀⣳⠏⠀⠀⠀⣸⠏⠀⣰⢏⠀⠀⠀⣰⠿⠀⠀⠀⣰⠏⠀⠀⠀⠀
	⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀       ⠀⠀⠐⡳⡦⡤⠶⡷⠀⠀⠐⠳⡦⡦⡦⠳⠀⠀⠀⠳⡦⡆⡦⠳⠁⠀⠀⠀⠰⡳⡦⡤⠆⠀
EOF
	_updoot_up
}

###################################
# Load Plugins
# loads plugins configured at
# top of file
###################################

for plugin in $plugin_files; do
	source $plugin
done


###################################
# ██       █      
# █ █ ███ ███ ███ 
# █ █ █ █  █  █   
# █ █ █ █  █    █ 
# ██  ███  ██ ███ Tool
# helper functions to setup
# and configure dots command
# !dots
###################################

function _dots_alias_init() {
	alias dots='git --git-dir=$DOTS_DIR --work-tree=$HOME'
	alias dotsl="dots log -n 20 --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit"
	compdef dots='git'
}

function _dots_help() {
	printf "%s\n\t%s\n\n\t%s\n\t\t%s\n\t%s\n\t\t%s\n\t\t%s\n\t%s\n\t\t%s\n\t\t%s\n" \
		"dots [operation] <operation settings>" \
			"function to help setup local dotfiles" \
			"branches [repo]" \
				"repo   - url to get branch list from" \
			"clone [branch] [repo]" \
				"branch - name of dots branch you want to copy" \
				"repo   - url for source git repo" \
			"new [branch] [repo]" \
				"branch - name of dots branch you are making" \
				"repo   - url for source git repo"
}

function _dots_post() {
	unset -f dots
	_dots_alias_init
}

function _dots_new() {
	WORKDIR=$(mktemp -d)

	NOTIFY "Creating new dotfile branch ${F[12]}$1$RESET" 

	git clone --recursive --branch blank "$2" $WORKDIR
	cd $WORKDIR
	git checkout -b "$1" 2>/dev/null
	echo "# $1:u" > README.md

	if [ "$(git config --global user.email 2>/dev/null)" = "" ]; then
		vared -p "git email not found, please enter it now: " -c GIT_EMAIL
		git config --global user.email "$GIT_EMAIL"
	fi

	if [ "$(git config --global user.name 2>/dev/null)" = "" ]; then
		vared -p "git name not found, please enter it now: " -c GIT_NAME
		git config --global user.name "$GIT_NAME"
	fi
	
	git add -u 2>/dev/null
	git commit -m "Initialize $1 dots" 2>/dev/null
	while ! git push origin "$1" 2>/dev/null; do
		WARN "Failed to authenticate, please try again."
	done

	cd $HOME
	rm -rf $WORKDIR
}

function _dots_clone() {
	WORKDIR=$(mktemp -d)

	NOTIFY "Cloning remote branch ${F[12]}$1$RESET"
	
	git clone --recursive --branch "$1" --single-branch --separate-git-dir="$DOTS_DIR" "$2" $WORKDIR
	rsync -rvl --exclude ".git" "$WORKDIR/" $HOME/ 2>/dev/null
	rm -r $WORKDIR
}

function _dots_branches() {
	git ls-remote --heads $1 | \
	while read sha branch; do
		branch=${branch##*/}
		if [ ! "$branch" = "blank" ]; then
			echo "$branch"
		fi
	done
}

if [ -d "$DOTS_DIR" ]; then
	_dots_alias_init
else
	NOTIFY "dotfiles not configured, type ${F[12]}dots${RESET} to see usage"
	function dots() {
		case $1 in
		"clone")
			[ $# -ne 3 ] &&
				_dots_help &&
				return 1
			_dots_clone $2 $3
			_dots_post
			;;
		"new")
			[ $# -ne 3 ] &&
				_dots_help &&
				return 1
			_dots_new $2 $3
			_dots_clone $2 $3
			_dots_post
			;;
		"branches")
			[ $# -ne 2 ] &&
				_dots_help &&
				return 1
			_dots_branches $2
			;;
		* )
			_dots_help $2 $3
			;;
		esac
	}
fi

if [ -f "$ZSH_EXTRA" ]; then
	source "$ZSH_EXTRA"
else
	NOTIFY "zsh extra file created, ${F[12]}$ZSH_EXTRA${RESET} this file should remain untracked and used for single systems."
	touch "$ZSH_EXTRA"
fi

###################################
# ███                    █  
# █ █ ███ ███ █████ ███ ███ 
# ███ █   █ █ █ █ █ █ █  █  
# █   █   █ █ █ █ █ █ █  █  
# █   █   ███ █ █ █ ███  ██ 
#                   █       
#                   █       
# my minimalist prompt
# !prompt
###################################

compress_pwd() {
    curWorkDir="$(pwd)"
    workDir=(${(s:/:)curWorkDir})

    index=0

    if (( ${+workDir[1]} )) && (( ${+workDir[2]} )); then
        if [ $workDir[1] = "home" ] && [ $workDir[2] = "$USER" ]; then
            echo -n "~"
            shift workDir; shift workDir
            atHome=true
        fi
    fi

    size=${#workDir[@]}
    if [ $size != 0 ]; then
        finalElement=${workDir[-1]}
        workDir=(${workDir:0:-1})
    fi

    if [ $size = 0 ] && [ ! $atHome ]; then
        echo -n "/"
    fi

    for element in $workDir; do
        if [ ${element:0:1} = "." ]; then
            echo -n "/${element:0:2}"
        else
            echo -n "/${element:0:1}"
        fi
    done

    if (( ${+finalElement} )); then
        echo -n "/$finalElement"
    fi
}

prompt_error() {
    RETVAL=$?

    if [ ! "$RETVAL" = "0" ]; then
        echo -n "%F{9}$RETVAL%f"
    fi
}

PROMPT=$(
    echo '%F{14}$(compress_pwd)%F{10}$(__git_ps1 ":%s") %F{11}${USER:0:1}%f@%F{12}%m%f '
)

RPROMPT='$(prompt_error)'

#if [[ ! -z "$ZSH_UPDATE" ]]; then
#	NOTIFY "Update to zshrc v$ZSH_UPDATE pending. To reload changes, run: reloadzsh"
#fi

# echo $(shuf -n 1 $HOME/.config/v/quotes)
