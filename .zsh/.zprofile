if systemctl -a is-active graphical.target && [[ ! $DISPLAY && $XDG_VTNR -eq 1 ]] && [[ "$(tty)" = "/dev/tty1" ]]; then
	exec startx
fi

export EDITOR=micro