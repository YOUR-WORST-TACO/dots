# brightness-control

- light

# colorpane

none

# docmatic

- xclip

# rofi_confirm

- rofi

# rofi_logout

- rofi

# select_kitty

- bc
- kitty
- wmctrl
- bspwm
- slop

# vlock

- imagemagick
- i3lock-color

# volume-control

- pamixer