#!/usr/bin/env bash

if pgrep -u $UID -x polybar >/dev/null; then
	killall -q polybar
	#while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done
	bspc config bottom_padding 0
else
	bspc config bottom_padding 42
	polybar right >>/tmp/polybar-right.log 2>&1 &
	polybar center >>/tmp/polybar-center.log 2>&1 &
	polybar left >>/tmp/polybar-left.log 2>&1 &
fi